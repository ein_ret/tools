# -*- coding: utf-8 -*-
#Author : ein

import threading

#This class make generator thread-safe by putting a lock on the next method
class generator_thread_safe():
    def __init__(self,it):
        self.it=it#.__iter__()
        self.lock=threading.Lock()

    def __iter__(self):
        return self

    def next(self):
        with self.lock:
            return next(self.it)

    __next__ = next

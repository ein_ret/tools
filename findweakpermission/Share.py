# -*- coding: utf-8 -*-
#Author : ein

import os

#Class to get the share available on the server
class share():
    def __init__(self,server):
        self.fserver=server
        self.lshare=list()

    def listshare(self):
        lserver=list()

        with open(self.fserver) as file:
            lserver=[line.rstrip('\n') for line in file.readlines()]

        for server in lserver :
            server='\\\\'+server
            shares=os.popen('net view '+server).readlines()
            shares=shares[7:-2]
            self.lshare.append([server+'\\'+line.split(' ')[0] for line in shares])

        return self.lshare

# -*- coding: utf-8 -*-
#Author : ein

import sys, traceback
from Crawler import crawler
from Permissions import permissions
from GeneratorThreadSafe import generator_thread_safe
from Share import share

if __name__ == "__main__":
    print ("[+] crawl shares...")

    share=share('inputs.lst')
    listshareserver=share.listshare()

    #Parsing the array of shares
    for sserver in listshareserver:
        #if no share we exit and take the next array
        if not sserver:
            break
        for share in sserver :
            print("\t [+] check permissions in share {}\n\n".format(share))
            try :
                #Initiate the crawler and make the generator threat safe
                mycrawler=crawler(share)
                itfile=mycrawler.get_itfile()
                itsafe=generator_thread_safe(itfile)
                NUMTHREAD=10
                threads=[permissions(itsafe) for x in range(NUMTHREAD)]

                #Time to thread for checking the file permissions
                for t in threads:
                    t.start()

                for t in threads:
                    t.join(60)

            except TypeError:
                print("exception handle {} - msg - ".format(sys.exc_info()[0]))
                tb=sys.exc_info()[2]
                print("exception {}".format(traceback.print_tb(tb)))
                pass

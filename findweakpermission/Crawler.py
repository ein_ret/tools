# -*- coding: utf-8 -*-
#Author : ein

import threading,win32security,os,logging

class crawler():
    def __init__(self,share):
        self.share=share
        self.filter=(".sql",".ps1",".bat",".ftp",".sftp",".key",".pst") #can be edited to add your extension

        #init func
        self.itdir=os.walk(self.share)
        self.itfile=self.get_file()

    def get_itfile(self):
        return self.itfile

#define a generator who return the next file available on the share.
#This generator is not thread-safe
#I'll use it through GeneratorThreadSafe to add Lock on next() method
    def get_file(self) :
        for root, dirs, files in self.itdir:
            for name in files :
                file=os.path.join(root,name)
                if file.endswith(self.filter):
                    #print (file)
                    yield file

# -*- coding: utf-8 -*-
#Author : ein

import win32security,os,logging,sys,traceback,time
from threading import Thread

#Class to check the file permissions permissions multi-threaded by using the dacl and apply a mask to get back the read and execute access
class permissions(Thread):
    def __init__(self,it):
        Thread.__init__(self)
        self.it=it
        self.output="output\\weakfiles_"+time.strftime("%Y-%m-%d_%H-%M-%S")+".log"
        self.logger=logging.basicConfig(filename=self.output,level=logging.INFO, format='%(asctime)s | %(threadName)s | %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        self.typical_perms={
            2032127:"Full Control(All)",
            1179817:"Read(RX)",
            1180086:"Add",
            1180095:"Add&Read",
            1245631:"Change"
            }
        self.user_search=["BUILTIN\\Users","\\Everyone"]

    def run(self):
        for file in self.it:
            print(self.name,file)
            perms=dict()
            lfiles=dict()
            flags = win32security.OWNER_SECURITY_INFORMATION | win32security.GROUP_SECURITY_INFORMATION | win32security.DACL_SECURITY_INFORMATION

            try:
                sd=win32security.GetFileSecurity(file, flags)
                ownersid=sd.GetSecurityDescriptorOwner()
                dacl=sd.GetSecurityDescriptorDacl()
                count = dacl.GetAceCount()
                for i in range(count):
                    ace = dacl.GetAce(i)
                    user, domain, int = win32security.LookupAccountSid(None, ace[2])
                    perms[domain+"\\"+user] = ace[1]

                for (key,val) in perms.items():
                    if ((key in self.user_search) and (val&1179817 == 1179817)):
                        oldvalue=lfiles.get(file)
                        if oldvalue is not None:
                            lfiles[file].append([key,self.typical_perms.get(val)])
                        else :
                            lfiles[file]=[[key,self.typical_perms.get(val)]]
                        print (file,lfiles[file])
                        logging.info("{} | {}".format(file,lfiles[file]))
            except AttributeError:
                tb=sys.exc_info()[2]
                print("exception {}".format(traceback.print_tb(tb)))
                pass

#!/bin/bash

if [ $# -ne 2 ]; then
    echo -e "illegal number of parameters;\n\n$0 <dictionnary_name to process> <output_name>\n"
    exit 0
fi

if [ ! $(which john) ]; then
    echo -e "Add path of john the ripper binary in your \$PATH"
    exit 0
fi

export JOHN=$(which john)


$JOHN -rules:gpocustom -w="$1" -stdout > short_wl_gpocustom.txt
$JOHN -external:Filter_gpocustom -w="short_wl_gpocustom.txt" -stdout > short_wl_gpocustom_filtered.txt
cat short_wl_gpocustom_filtered.txt | sort | uniq > $2

rm short_wl_gpocustom.txt short_wl_gpocustom_filtered.txt
